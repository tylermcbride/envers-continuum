package cse.concert.tyler.envers.domain;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Audited
@Entity(name = "ANOTHER_TEST")
public class AnotherTest {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String auditedField;
}