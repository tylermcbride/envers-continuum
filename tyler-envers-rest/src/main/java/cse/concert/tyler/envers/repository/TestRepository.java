package cse.concert.tyler.envers.repository;

import cse.concert.tyler.envers.domain.Test;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<Test, Long> {
}
