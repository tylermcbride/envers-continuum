package cse.concert.tyler.envers.repository;

import cse.concert.tyler.envers.domain.AnotherTest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnotherTestRepository extends JpaRepository<AnotherTest, Long> {
}
