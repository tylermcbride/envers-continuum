package cse.concert.tyler.envers.controllers;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import cse.concert.tyler.envers.domain.AnotherTest;
import cse.concert.tyler.envers.domain.Test;
import cse.concert.tyler.envers.repository.AnotherTestRepository;
import cse.concert.tyler.envers.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/envers")
public class EnversController {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private TestRepository repository;

    @Autowired
    private AnotherTestRepository anotherTestRepository;

    @PatchMapping("/test")
    public Test index(@RequestBody Test test, @RequestParam("comments") String comments) {
        return repository.save(test);
    }

    @PatchMapping("/anotherTest")
    public AnotherTest index(@RequestBody AnotherTest test, @RequestParam("comments") String comments) {
        return anotherTestRepository.save(test);
    }
}
